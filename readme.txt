
/*

* Drupal Module: SocialShareButtons
 
* Adds the required Javascript to the bottom of all your Drupal pages
 
* to allow tracking by the Social Share Buttons statistics package.
 
* @author: Raghunath Gujrar <http://drupal.org/user/85918>
 */
* @description : 
     "social_share_buttons" is the very simple module for add to social buttons on your site(floating sidebar).

     Using this module you can easly share your website with your firends on Facebook,Twitter,Linkedin,Pinterest,Google+.

     There are not added any external JS files in this module so it's does not effect on your site speed as well.
* @installation: 
      Step 1. Download "social_share_buttons" module and upload folder into to  `sites/all/module/` directory
      Step 2. Activate the module from admin
**/